package ocelot.desktop.util

object Orientation extends Enumeration {
  val Vertical, Horizontal = Value
}
