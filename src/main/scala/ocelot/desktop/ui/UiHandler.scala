package ocelot.desktop.ui

import java.io.{File, FileOutputStream}
import java.nio.ByteBuffer
import java.nio.channels.Channels
import java.nio.file.{Files, Paths}

import javax.imageio.ImageIO
import ocelot.desktop.OcelotDesktop
import ocelot.desktop.geometry.{Size2D, Vector2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.event.MouseEvent
import ocelot.desktop.ui.event.handlers.HoverHandler
import ocelot.desktop.ui.event.sources.{KeyEvents, MouseEvents, ScrollEvents}
import ocelot.desktop.ui.widget.{RootWidget, Widget}
import ocelot.desktop.util._
import org.apache.commons.io.FileUtils
import org.apache.commons.lang3.SystemUtils
import org.lwjgl.input.Mouse
import org.lwjgl.openal.{AL, AL10, ALC10}
import org.lwjgl.opengl._
import org.lwjgl.{BufferUtils, Sys}

import scala.collection.mutable

object UiHandler extends Logging {
  var root: RootWidget = _
  var graphics: Graphics = _

  private var hierarchy = mutable.ArrayBuffer(root: Widget)
  private var shouldUpdateHierarchy = true
  private val fpsCalculator = new FPSCalculator
  private var _windowTitle = "Ocelot Desktop"
  private var windowSize: Size2D = Size2D(0, 0)
  private val ticker = new Ticker
  ticker.tickInterval = 1000f / 60f

  def getHierarchy: Array[Widget] = hierarchy.toArray

  def updateHierarchy(): Unit = {
    shouldUpdateHierarchy = true
  }

  def _updateHierarchy(): Unit = {
    val stack = mutable.Stack(root: Widget)
    hierarchy.clear()
    while (stack.nonEmpty) {
      val widget = stack.pop()
      hierarchy += widget
      for (child <- widget.hierarchy.reverseIterator) {
        stack.push(child)
      }
    }

    shouldUpdateHierarchy = false
  }

  def fps: Float = fpsCalculator.fps

  def dt: Float = fpsCalculator.dt

  def mousePosition: Vector2D = {
    Vector2D(Mouse.getX, Display.getHeight - Mouse.getY)
  }

  def clipboard: String = Sys.getClipboard

  def init(root: RootWidget): Unit = {
    this.root = root
    root.relayout()
    root.size = Size2D(800, 600)

    loadLibraries()

    Display.setDisplayMode(new DisplayMode(root.size.width.toInt, root.size.height.toInt))
    Display.setTitle("Ocelot Desktop")
    loadIcons()
    Display.create()
    KeyEvents.init()
    MouseEvents.init()
    Display.setResizable(true)
    Display.setVSyncEnabled(true)

    windowSize = root.size.copy()

    logger.info(s"Created window with $windowSize")
    logger.info(s"OpenGL vendor: ${GL11.glGetString(GL11.GL_VENDOR)}")
    logger.info(s"OpenGL renderer: ${GL11.glGetString(GL11.GL_RENDERER)}")

    Spritesheet.load()
    graphics = new Graphics

    AL.create()
    AL10.alGetError()
    logger.info(s"OpenAL device: ${ALC10.alcGetString(AL.getDevice, ALC10.ALC_DEVICE_SPECIFIER)}")
  }

  var tempDir: String = _

  private def loadLibraries(): Unit = {
    tempDir = Files.createTempDirectory("ocelot-desktop").toString

    val arch = System.getProperty("os.arch")
    val is64bit = arch.startsWith("amd64")

    val libs = if (SystemUtils.IS_OS_WINDOWS)
      if (is64bit)
        Array("jinput-dx8_64.dll", "jinput-raw_64.dll", "jinput-wintab.dll", "lwjgl64.dll", "OpenAL64.dll")
      else
        Array("jinput-dx8.dll", "jinput-raw.dll", "jinput-wintab.dll", "lwjgl.dll", "OpenAL32.dll")
    else if (SystemUtils.IS_OS_MAC_OSX)
      Array("liblwjgl.dylib")
    else if (SystemUtils.IS_OS_LINUX)
      if (is64bit)
        Array("libjinput-linux64.so", "liblwjgl64.so", "libopenal64.so")
      else
        Array("libjinput-linux.so", "liblwjgl.so", "libopenal.so")
    else
      throw new Exception("Unsupported OS")

    for (lib <- libs) {
      val source = getClass.getResourceAsStream("/" + lib)
      val dest = new File(Paths.get(tempDir, lib).toString)
      val output = new FileOutputStream(dest)
      output.getChannel.transferFrom(Channels.newChannel(source), 0, Long.MaxValue)
      output.flush()
      output.close()
      source.close()
    }

    System.setProperty("org.lwjgl.librarypath", tempDir)
  }

  private def loadIcons(): Unit = {
    val sizes = Array(256, 128, 64, 32, 16)
    val list = new Array[ByteBuffer](sizes.length)

    for ((size, i) <- sizes.zipWithIndex) {
      val imageURL = getClass.getResource(s"/ocelot/desktop/icon$size.png")
      val image = ImageIO.read(imageURL)
      val pixels = new Array[Int](image.getWidth * image.getHeight)
      image.getRGB(0, 0, image.getWidth, image.getHeight, pixels, 0, image.getWidth)
      val buf = BufferUtils.createByteBuffer(image.getWidth * image.getHeight * 4)

      for (y <- 0 until image.getHeight) {
        for (x <- 0 until image.getWidth) {
          val pixel = pixels(y * image.getWidth + x)
          buf.put(((pixel >> 16) & 0xFF).toByte)
          buf.put(((pixel >> 8) & 0xFF).toByte)
          buf.put((pixel & 0xFF).toByte)
          buf.put(((pixel >> 24) & 0xFF).toByte)
        }
      }

      buf.flip()
      list(i) = buf
    }

    Display.setIcon(list)
    logger.info(s"Loaded window icons of sizes ${sizes.mkString(", ")}")
  }

  def windowTitle: String = _windowTitle

  def windowTitle_=(title: String): Unit = {
    Display.setTitle(title)
    _windowTitle = title
  }

  private var exitRequested = false
  private var exitHandlerCalled = false

  def exit(): Unit = {
    exitRequested = true
  }

  def start(): Unit = {
    while (!exitRequested) {
      if (!exitHandlerCalled && Display.isCloseRequested) {
        exitHandlerCalled = true
        OcelotDesktop.exit()
      }

      Audio.update()

      updateWindowSize()
      KeyEvents.update()
      MouseEvents.update()

      update()
      draw()

      ticker.waitNext()
      Display.update()
      fpsCalculator.tick()
    }
  }

  def terminate(): Unit = {
    KeyEvents.destroy()
    MouseEvents.destroy()
    Display.destroy()
    AL.destroy()

    FileUtils.deleteDirectory(new File(tempDir))
  }

  private def update(): Unit = {
    val mousePos = mousePosition
    if (mousePos.x < 0 || mousePos.y < 0 || mousePos.x > windowSize.width || mousePos.y > windowSize.height) {
      KeyEvents.releaseKeys()
      MouseEvents.releaseButtons()
    }

    if (shouldUpdateHierarchy) _updateHierarchy()

    for (event <- KeyEvents.events)
      hierarchy.reverseIterator.foreach(_.handleEvent(event))

    for (event <- MouseEvents.events)
      hierarchy.reverseIterator.filter(_.receiveAllMouseEvents).foreach(_.handleEvent(event))

    val scrollTarget = hierarchy.reverseIterator
      .find(w => w.receiveScrollEvents && w.clippedBounds.contains(mousePos))

    val mouseTarget = hierarchy.reverseIterator
      .find(w => w.receiveMouseEvents && w.clippedBounds.contains(mousePos))

    for (event <- ScrollEvents.events)
      scrollTarget.foreach(_.handleEvent(event))

    for (event <- MouseEvents.events)
      if (event.state == MouseEvent.State.Press) {
        mouseTarget.foreach(_.handleEvent(event))
      } else
        hierarchy.reverseIterator.foreach(_.handleEvent(event))

    hierarchy.reverseIterator.foreach {
      case handler: HoverHandler if !mouseTarget.contains(handler) => handler.setHovered(false)
      case _ =>
    }

    mouseTarget.foreach {
      case handler: HoverHandler => handler.setHovered(true)
      case _ =>
    }

    root.update()
  }

  private def updateWindowSize(): Unit = {
    windowSize = Size2D(Display.getWidth, Display.getHeight)
    root.size = windowSize.copy()
    graphics.resize(windowSize.width.toInt, windowSize.height.toInt)
  }

  private def draw(): Unit = {
    graphics.setViewport(windowSize.width.asInstanceOf[Int], windowSize.height.asInstanceOf[Int])
    graphics.clear()
    root.draw(graphics)

    graphics.flush()
    graphics.update()
  }
}
